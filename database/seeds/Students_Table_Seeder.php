<?php

use Illuminate\Database\Seeder;

class Students_Table_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for($i=1;$i<=5000;$i++){
          $arrData=[
              "name"=>str_random(10),
              "roll"=>str_random(10),
              "result"=>str_random(10),
          ];

          DB::table('students')->insert($arrData);
      }

    }
}
